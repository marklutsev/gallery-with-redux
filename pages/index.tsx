import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.sass'
import React from "react";

// Actions
import { showPreviousPhoto } from '../source/actions';
import { showSelectedPhoto } from '../source/actions';
import { showNextPhoto } from '../source/actions';

// Store
import { store } from '../source/store';

export default class Gallery extends React.Component {
    
    _showPreviousPhoto = () => {
        store.dispatch(showPreviousPhoto());
        this.forceUpdate();
    }

    _showSelectedPhoto = () => {
        store.dispatch(showSelectedPhoto(event.target.value));
        console.log(store.getState().selectedPhotoIndex);
        this.forceUpdate();
    }

    _showNextPhoto = () => {
        store.dispatch(showNextPhoto());
        this.forceUpdate();
    }

    render() {

        const state: any  = store.getState();
        const selectedPhoto = state.photos.find((_: any,  photoIndex: number) => photoIndex === state.selectedPhotoIndex);


        return (
            <div>
                <Head>
                    <title>Gallery</title>
                    <link rel="shortcut-icon" href="../public/gallery.ico" />
                </Head>
                <section className={styles.gallery}>
                    <Image src={selectedPhoto.url} width="400px" height="200px" alt="galleryPhoto" className={styles.galleryImage} />
                    <ul className={styles.buttonList}>
                        <li className={styles.element}>
                            <button className={styles.button} onClick={() => this._showPreviousPhoto()}>L</button>
                        </li>
                        <li className={styles.element}>
                            <button className={styles.button} value='0' onClick={() => this._showSelectedPhoto()}>1</button>
                        </li>
                        <li className={styles.element}>
                            <button className={styles.button} value='1' onClick={() => this._showSelectedPhoto()}>2</button>
                        </li>
                        <li className={styles.element}>
                            <button className={styles.button} value='2' onClick={() => this._showSelectedPhoto()}>3</button>
                        </li>
                        <li className={styles.element}>
                            <button className={styles.button} value='3' onClick={() => this._showSelectedPhoto()}>4</button>
                        </li>
                        <li className={styles.element}>
                            <button className={styles.button} onClick={() => this._showNextPhoto()}>R</button>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}