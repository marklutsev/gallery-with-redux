import { createStore, applyMiddleware, compose } from "redux";

// Reducer
import { galleryReducer } from "./reducer";

const preloadedState = JSON.parse(localStorage.getItem("gallery"));
export const store = preloadedState ? createStore(galleryReducer, preloadedState) : createStore(galleryReducer);

store.subscribe(() => {
    const state: any = store.getState();

    localStorage.setItem("gallery", JSON.stringify(state));
});