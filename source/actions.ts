// Types
import { SHOW_PREVIOUS_PHOTO } from "./types";
import { SHOW_SELECTED_PHOTO } from "./types";
import { SHOW_NEXT_PHOTO } from "./types";

// Actions
export const showPreviousPhoto = (): {type: string} => {
    return {
        type: SHOW_PREVIOUS_PHOTO
    };
}

export const showSelectedPhoto = (photoIndex: string): {type: string, payload: string} => {
    return {
        type: SHOW_SELECTED_PHOTO,
        payload: photoIndex
    };
}

export const showNextPhoto = (): {type: string} => {
    return {
        type: SHOW_NEXT_PHOTO
    };
}