// Types
import { SHOW_PREVIOUS_PHOTO } from "./types";
import { SHOW_SELECTED_PHOTO } from "./types";
import { SHOW_NEXT_PHOTO } from "./types";

// Images
import photo1 from "../public/assets/a.jpg";
import photo2 from "../public/assets/b.jpg";
import photo3 from "../public/assets/c.jpg";
import photo4 from "../public/assets/d.jpg";

const initialState: {photos: {id: number, url: StaticImageData}[], selectedPhotoIndex: number} = {
    photos: [
        {id: 1, url: photo1},
        {id: 2, url: photo2},
        {id: 3, url: photo3},
        {id: 4, url: photo4}
    ],
    selectedPhotoIndex: 0
};

export const galleryReducer = (state=initialState, action: {type: string, payload?: string}): {photos: {id: number, url: StaticImageData}[], selectedPhotoIndex: number} => {
    switch (action.type) {
        case SHOW_PREVIOUS_PHOTO:
            if (state.selectedPhotoIndex === 0) {
                return state;
            }

            return {
                ...state,
                selectedPhotoIndex: state.selectedPhotoIndex - 1
            };

            break;

        case SHOW_SELECTED_PHOTO:
            return {
                ...state,
                selectedPhotoIndex: parseInt(action.payload)
            };
    
            break;

        case SHOW_NEXT_PHOTO:
            if (state.selectedPhotoIndex === state.photos.length - 1) {
                return state;
            }

            return {
                ...state,
                selectedPhotoIndex: state.selectedPhotoIndex + 1
            };

            break;
        
        default:
            return state;
    }
}